import index
import unittest

class TestApp(unittest.TestCase):

    def setUp(self):
        index.app.testing = True
        self.app = index.app.test_client()

    def test_index(self):
        result = self.app.get('/')
        self.assertEqual(b'Hello World!', result.data)
